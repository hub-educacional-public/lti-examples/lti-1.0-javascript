## HUB LTI Signature

Projeto de exemplo utilizando LTI(Learning Tool Interoperability) criado pela organização IMS Global Learning Consortium, inclui um protocolo padrão estabelecer um relacionamento de confiança entre o provedor da ferramenta e o Sistema de Gerenciamento de Aprendizagem.

---

![Alt-Text](https://gitlab.com/positivote/hub-educacional/lti-example/uploads/86d47558b8358992c6705fb54b35c322/main-logo.png)

---

### Request Signature

Chamada para assinar e retornar um pacote LTI assinado.

```
POST https://apihub.educacional.com/hub/qa/v1/external/lti/signature
```

### Headers
```json
{
  "X-Api-Key": "RDfFL1kKkBN.GHD-SDT8C3FqKknLKDHNSHY_5-STOoBDC4oZlxj-TRG",
  "Hub-App-Id": "99",
  "School-Inep": "13082175",
  "Hub-User-Code": "Aluno965696",
  "Custom-Activity-Id": "*",
  "Custom-Launch-Method": null, //Campo Opcional
  "Custom-Redirect-Url": null, //Campo Opcional
}
```

* **X-Api-Key:** Identificador exclusivo fornecido pela equipe de Hub usado para autenticação da chamada da api.

* **Hub_App_id:** identificador único do App fornecido pela equipe do Hub, este identificador deve estar vinculado a tarefa da trilha.

* **School-Inep:** o código INEP/MEC de sua instituição de ensino. Esse código permite identificar de maneira unificada cada uma das escolas do Brasil, de acordo com o cadastro junto ao Ministério da Educação – devendo conter oito caracteres numéricos  ( exemplo: "13082175").

* **Hub-User-Code:** Identificador único do usuário no Hub

* **Custom-Activity-Id:** Identificador da página em que se encontra as atividades a serão executadas pelo usuário.

* **Custom-Launch-Method:** É o método do formulario utilizado GET ou POST, se informada sobrescreve a original.

* **Custom-Redirect-Url:** É a uri da aplicação (Action do Formulario), se informada sobrescreve a original.

### Request

* Exemplo retornando uma validação de pacote LTI.

Code 200
Status OK

```json
{
  "data": {
    "action": "https:/apihub.educacional.com/qa/v1/validate",
    "method": "POST",
    "params": {
      "lti_message_type": "basic-lti-launch-request",
      "lti_version": "LTI-1p0",
      "resource_link_id": "LTI-usage",
      "user_id": "F8FFC022-2D25-11EB-BDBD-0A190BAE2C86",
      "custom_product": "app name",
      "lis_person_name_given": "GABRIEL",
      "lis_person_contact_email_primary": "gabriel@hub.com",
      "lis_person_name_family": "CESÁRIO",
      "roles": "aluno",
      "custom_school_user_id": "20180131",
      "custom_school_hub_id": "89cd3b7b-7e2e-4309-b192-41a807bb6b0d",
      "app_id": "4BDA0C2F-452A-11EB-BDBD-0A190BAE2C86",
      "custom_activity_id": null,
      "oauth_nonce": 1613756961,
      "oauth_timestamp": 1613756961,
      "oauth_body_hash": "2jmj7l5rSw0yVb/vlWAYkK/YBwk=",
      "oauth_consumer_key": "hub.educacional.com",
      "oauth_token": "",
      "oauth_signature_method": "HMAC-SHA1",
      "oauth_signature": "z+fENfdD6fEj0/yR9fcKBJANWVc="
    }
  }
}
```

* **user_id:** ID único do usuário no HUB

* **custom_activity_id:** ID da atividade em que o app pode ser aberto

* **custom_school_user_id:** Código do usuário enviado pela escola

* **action:** Retorna a URL de callback que referência a pagina a ser carregada

### Erros
Code 403:

* Erro ao validar o pacote LTI

Code 422:

 * Pacote LTI não assinado

Code 401:

 * Unauthorized.

Code 422:

 * Aplicativo não existe
 * Usuàrio Hub não existe
 * Instituição não existe
 * Erro ao assinar pacote LTI

### Request Validade

* Com o resultado do response do endpoint "https://apihub.educacional.com/hub/qa/v1/external/lti/signature" Os dados devem ser enviados para a url de callback retornada utilizando o formato abaixo.

* URL de callback de exemplo

```
POST https://apihub.educacional.com/hub/qa/v1/external/lti/validate

```

* Formato a ser enviado o payload 

```
Content-Type    multipart/form-data
```

![Alt-Text](https://gitlab.com/positivote/hub-educacional/lti-example/uploads/603ee16164466761216748e03076c7e0/logo-powered-by-hub.png)
