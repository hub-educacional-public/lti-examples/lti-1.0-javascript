const App = (() => {
  /**
   * Constants Let :)
   */
  let X_API_KEY             = sessionStorage.getItem('X-API-KEY') || "q9JXx-eiXtY.OIY-te79VB_vmLikJLJgb76Xs7IYVg8a86E-z8o4s0s";
  let USER_HUB_CODE         = sessionStorage.getItem('USER-HUB-CODE') || "ALU002";
  let HUB_INSTITUTION_CODE  = sessionStorage.getItem('HUB-INSTITUTION-CODE') || ""
  let SCHOOL_INEP           = sessionStorage.getItem('SCHOOL-INEP') || ""
  let CUSTOM_ACTIVITY_ID    = sessionStorage.getItem('CUSTOM-ACTIVITY-ID') || "";
  let CUSTOM_LAUNCH_URL   = sessionStorage.getItem('CUSTOM-REDIRECT-URL') || "";
  let CUSTOM_LAUNCH_METHOD  = sessionStorage.getItem('CUSTOM-LAUNCH-METHOD') || "";
  let CUSTOM_URL_REDIRECT   = sessionStorage.getItem('CUSTOM_URL_REDIRECT') || "";

  /**
   * Rxjs
   */
  const { Subject, fromEvent, of, EMPTY, throwError } = rxjs;
  const { tap, map, switchMap, filter, distinctUntilChanged, catchError } = rxjs.operators;

  /** Global Subject */
  const onMessage$ = new Subject();
  const onMessage = onMessage$.asObservable();
  
  /**
   * Services
   */
  class _ApiService {

    baseUrl = "";

    constructor() {}

    get (endpoint, overrides) {
      overrides = overrides || ((headers) => headers);

      return fetch(new Request(`${this.baseUrl}/${endpoint}`, {
        method: 'GET',
        mode: 'cors',
        cache: 'no-cache',
        headers: overrides(this.defaultHeaders)
      }));
    }

    post (endpoint, body, overrides) {
      overrides = overrides || ((headers) => headers);

      return fetch(new Request(`${this.baseUrl}/${endpoint}`, {
        method: 'POST',
        body: body,
        mode: 'cors',
        cache: 'no-cache',
        headers: overrides(this.defaultHeaders)
      }));
    }

    get defaultHeaders() {
      return new Headers({
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "X-Api-Key": X_API_KEY
      })
    }
  }

  class _AppService extends _ApiService {
    constructor() {
      super();
      this.baseUrl = "https://apihub.educacional.com/lti/v1"
    }

    shortcuts() {
      return this.get('external/shortcuts', (headers) => {
        if (!isEmpty(SCHOOL_INEP)) headers.set("School-Inep", SCHOOL_INEP);
        if (!isEmpty(USER_HUB_CODE)) headers.set("Hub-User-Code", USER_HUB_CODE);
        return headers;
      });
    }
  }
  const AppService = new _AppService();

  class _LTISignatureService extends _ApiService {
    constructor() {
      super();
      this.baseUrl = "https://apihub.educacional.com/lti/v1"
    }

    signature(data) {
      return this.post('external/lti/signature', toJson({}), (headers) => {
        headers.set("Hub-App-Id", data.appId);
        if (!isEmpty(SCHOOL_INEP)) headers.set("School-Inep", SCHOOL_INEP);
        if (!isEmpty(USER_HUB_CODE)) headers.set("Hub-User-Code", USER_HUB_CODE);
        if (!isEmpty(HUB_INSTITUTION_CODE)) headers.set("Hub-Institution-Code", HUB_INSTITUTION_CODE);
        if (!isEmpty(CUSTOM_ACTIVITY_ID)) headers.set("Custom-Activity-Id", CUSTOM_ACTIVITY_ID);
        if (!isEmpty(CUSTOM_LAUNCH_METHOD)) headers.set("Custom-Launch-Method", CUSTOM_LAUNCH_METHOD);
        if (!isEmpty(CUSTOM_LAUNCH_URL)) headers.set("Custom-Launch-Url", CUSTOM_LAUNCH_URL);
        if (!isEmpty(CUSTOM_URL_REDIRECT)) headers.set("Custom-Url-Redirect", CUSTOM_URL_REDIRECT);
        return headers;
      });
    }
  }
  const LTISignatureService = new _LTISignatureService();

  class _LTIValidatorService extends _ApiService {
    constructor() {
      super();
      this.baseUrl = "https://apihub.educacional.com/lti/v1"
    }

    validate(params) {
      return this.post('external/lti/validate', params);
    }
  }
  const LTIValidatorService = new _LTIValidatorService();

  /**
   * Components
   */
  class AbstractComponent extends HTMLElement {
    constructor() {
      super();
      this.subscriptions = [];
    }

    disconnectedCallback() {
      this.subscriptions.map(subscription => subscription.unsubscribe());
    }
  }

  class SettingComponent extends AbstractComponent {
    get apiKeyCtrl() { return this.querySelector('#api-key') }
    get userCodeCtrl() { return this.querySelector('#user-code') }
    get institutionCodeCtrl() { return this.querySelector('#institution-code') }
    get inepCtrl() { return this.querySelector('#inep') }
    get activityIdCtrl() { return this.querySelector('#activity-id') }
    get redirectUrlCtrl() { return this.querySelector('#redirect-url') }
    get launchMethodCtrl() { return this.querySelector('#launch-method') }
    get launchUrlCtrl() { return this.querySelector('#launch-url') }

    get confirmButton () { return this.querySelector('#confirm') }

    constructor() {
      super();

      this.storage = window.sessionStorage;
      this.onMessage$ = onMessage$;
    }

    connectedCallback() {
      this.innerHTML = `
        <ion-list>
          <ion-item>
            <ion-label color="medium" position="stacked">X-Api-Key</ion-label>
            <ion-input id="api-key" value="${X_API_KEY}" placeholder="${X_API_KEY}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">Hub-User-Code</ion-label>
            <ion-input id="user-code" value="${USER_HUB_CODE}" placeholder="${USER_HUB_CODE}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">Hub-Institution-Code (Opcional)</ion-label>
            <ion-input id="institution-code" value="${HUB_INSTITUTION_CODE}" placeholder="${HUB_INSTITUTION_CODE}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">School-Inep (Opcional)</ion-label>
            <ion-input id="inep" value="${SCHOOL_INEP}" placeholder="${SCHOOL_INEP}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">Custom-Activity-Id (Opcional)</ion-label>
            <ion-input id="activity-id" value="${CUSTOM_ACTIVITY_ID}" placeholder="${CUSTOM_ACTIVITY_ID}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">Custom-Url-Redirect (Opcional)</ion-label>
            <ion-input id="redirect-url" value="${CUSTOM_URL_REDIRECT}" placeholder="${CUSTOM_URL_REDIRECT}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">Custom-Launch-Url (Opcional)</ion-label>
            <ion-input id="launch-url" value="${CUSTOM_LAUNCH_URL}" placeholder="${CUSTOM_LAUNCH_URL}"></ion-input>
          </ion-item>
          <ion-item>
            <ion-label color="medium" position="stacked">Custom-Launch-Method (Opcional) [POST|GET]</ion-label>
            <ion-input id="launch-method" value="${CUSTOM_LAUNCH_METHOD}" placeholder="${CUSTOM_LAUNCH_METHOD}"></ion-input>
          </ion-item>
          <ion-button id="confirm" class="ion-margin" expand="block" fill="outline">
            Confirmar
          </ion-button>
        </ion-list>
      `;

      this.subscriptions.push(
        fromEvent(this.apiKeyCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('X-API-KEY', value);
              X_API_KEY = value
            })
          )
          .subscribe(),

        fromEvent(this.userCodeCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('HUB-USER-CODE', value);
              USER_HUB_CODE = value;
            })
          )
          .subscribe(),

        fromEvent(this.institutionCodeCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('HUB-INSTITUTION-CODE', value);
              HUB_INSTITUTION_CODE = value;
            })
          )
          .subscribe(),

        fromEvent(this.inepCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('SCHOOL-INEP', value);
              SCHOOL_INEP = value;
            })
          )
          .subscribe(),

        fromEvent(this.activityIdCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('CUSTOM-ACTIVITY-ID', value);
              CUSTOM_ACTIVITY_ID = value;
            })
          )
          .subscribe(),

        fromEvent(this.redirectUrlCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('CUSTOM-URL-REDIRECT', value);
              CUSTOM_URL_REDIRECT = value;
            })
          )
          .subscribe(),
        
        fromEvent(this.launchMethodCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('CUSTOM-LAUNCH-METHOD', value);
              CUSTOM_LAUNCH_METHOD = value;
            })
          )
          .subscribe(),

        fromEvent(this.launchUrlCtrl, 'ionInput')
          .pipe(
            map(event => event.target.value),
            distinctUntilChanged(),
            tap(value => {
              this.storage.setItem('CUSTOM-LAUNCH-URL', value);
              CUSTOM_LAUNCH_URL = value;
            })
          )
          .subscribe(),

        fromEvent(this.confirmButton, 'click')
          .subscribe(() => {
            this.onMessage$.next();
          })
      );

      setTimeout(() => {
        this.onMessage$.next();
      }, 1500)
    }
  }

  class AppItemComponent extends AbstractComponent {
    app = null;

    constructor() {
      super();

      this.ltiSignatureService = LTISignatureService;
      this.ltiValidatorService = LTIValidatorService;
    }
    
    connectedCallback() {
      const app = this.app;

      this.innerHTML = `
        <ion-item button disabled="${app.disabled}">
          <ion-avatar slot="start">
            <img src="${app.logoUrl}">
          </ion-avatar>
          <ion-label>
            <h2>${app.appName}</h2>
            <p>${app.appDescription}</p>
          </ion-label>
        </ion-item>
      `

      const item = this.querySelector('ion-item');
      this._bindEvent(item)
    }

    signPackage(data) {
      return this.ltiSignatureService.signature(data)
        .then(async (response) => {
          if (response.ok) {
            const { data } = await response.json();
            return data;
          }

          throw new Error('Erro ao assinar pacote LTI.');
        })
    }

    validateSignature(params) {
      return this.ltiValidatorService.validate(params)
        .then(async (response) => {
          if (response.ok) {
            const { data } = await response.json();
            return data;
          }

          throw new Error('Erro ao validar assinatura do pacote.')
        })
    }

    async _bindEvent(card) {
      const { appId, appName } = this.app;
      const getLoading = () => document.querySelector('ion-loading');

      const subscription = fromEvent(card, 'click')
        .pipe(
          switchMap(async () => {
            card.setAttribute('disabled', 'disabled');
            await presentLoading({
              message: `Assinando pacote LTI para '${appName}'...`
            });     
            logEvent('INFO', `Assinando pacote LTI para '${appName}'`);
            return this.signPackage({ appId });
          }),
          switchMap(lti => {
            logEvent('INFO', `LTI assinado para '${appName}'`, lti);
            const loading = getLoading();
            loading.message = `Abrindo aplicação '${appName}'...`;

            const form = document.createElement('form');
            form.style.display = 'none';
            form.id = 'lti-form';
            form.method = lti.method;
            form.action = lti.action;
            form.target = 'simulator';

            // form.addEventListener('formdata', (evt) => {
            //   evt.preventDefault()
            //   var request = new XMLHttpRequest();
            //   request.open("POST", "/formHandler");
            //   request.send(data);
            // })

            form.innerHTML = `
              ${lti.params && Object.entries(lti.params).map(([k, v]) => `
                <input type="hidden" name="${k}" value="${v ? v : ''}">
              `).join('')}

              <input type="submit" />
            `;

            document.body.appendChild(form);
            return of(lti);
          }),
          // switchMap((lti) => {
          //   const loading = getLoading();
          //   loading.message = `Validando pacote LTI para '${appName}'...`;

          //   logEvent('INFO', `Validando pacote LTI para '${appName}'`);
          //   return this.validateSignature(toJson(lti));
          // }),
          switchMap(async () => {
            const form = document.querySelector('#lti-form');
            const loading = getLoading();

            if (form) {
              loading.dismiss()
              const alert = await presentAlertRadio(
                'Abrir em:',
                [{
                  type: 'radio',
                  label: 'Iframe',
                  value: 'simulator',
                  icon: 'browsers-outline',
                  checked: true
                },
                {
                  type: 'radio',
                  label: 'Aba atual',
                  value: '_self'
                },
                {
                  type: 'radio',
                  label: 'Nova Aba',
                  value: '_blank'
                }],
                [{
                  text: 'Abrir',
                  handler: (evt) => {}
                }]
              )
              
              const { data } = await alert.onDidDismiss();

              if (data) {
                const { values: action } = data;
                form.target = action;

                const submit = form.querySelector('input[type="submit"]');
                submit.click();
              }
            }

            await presentLoading({
              message: `Aguarde...`
            });

            return of(true);
          }),
          catchError(async (error) => {
            const loading = getLoading();
            
            loading.message = `${error ? error.message : error}`;
            await sleep(2000);
            loading.dismiss();

            logEvent('ERROR', `${error ? error.message : error}`);
            return EMPTY;
          })
        ).subscribe(
          async () => {
            const form = document.querySelector('#lti-form');

            if (form) {
              const loading = getLoading();
              loading.dismiss();

              presentToast({
                message: `Aplicação '${appName}' iniciada com sucesso`,
                duration: 2500,
                buttons: [
                  { text: 'Fechar' }
                ]
              })

              await sleep(2000);
              form.remove();
            }

            logEvent('INFO', `Aplicação '${appName}' iniciada com sucesso`);
            card.removeAttribute('disabled');
          },
          (error) => {
            console.error(error)
            presentToast({
              message: `Erro ao iniciar aplicação`,
              duration: 2000,
              buttons: [
                { icon: 'close', color: 'danger' }
              ]
            });

            card.removeAttribute('disabled');
          }
        );

      this.subscriptions.push(subscription);
    }
  }  

  class AppListComponent extends AbstractComponent {

    get skeletonText() {
      return `<ion-list>
                <ion-item>
                  <ion-avatar slot="start">
                    <ion-skeleton-text animated style="width: 100%"></ion-skeleton-text>
                  </ion-avatar>
                  <ion-label>
                    <h2><ion-skeleton-text animated style="width: 30%"></ion-skeleton-text></h2>
                    <p><ion-skeleton-text animated style="width: 100%"></ion-skeleton-text></p>
                  </ion-label>
                </ion-item>
              </ion-list>`
    }

    get slot() { return this.querySelector('slot') }

    constructor() {
      super();
      this.appService = AppService;

      this.appList$ = new Subject();
      this.appList = this.appList$.asObservable();

      this.onMessage = onMessage;

      this.appList.subscribe(
        (apps) => {
          this.slot.innerHTML = ``;

          const ionList = document.createElement('ion-list');
          apps.map(app => {
            const appCard = document.createElement('app-card');
            appCard.app = app;

            ionList.appendChild(appCard);
          }); 
          
          if (!apps.length) {
            this.slot.innerHTML = `
              <ion-item>
                <ion-label>Nenhum aplicativo encontrado.</ion-label>
              </ion-item>
            `;
          }
          
          this.slot.appendChild(ionList);
        },
        (error) => {
          logEvent('ERROR', 'Erro ao listar aplicativos desta Instituição.');
          this.slot.innerHTML = `
            <ion-item>
              <ion-label>Nenhum aplicativo encontrado.</ion-label>
            </ion-item>
          `;
        }
      );

      this.onMessage
        .pipe(
          tap(() => this.slot.innerHTML = `${this.skeletonText}`),
          switchMap(() => this.getApps())
        )
        .subscribe()
    }

    connectedCallback() {
      this.innerHTML = `
        <slot>${this.skeletonText}</slot>
      `;
    }

    async getApps() {
      return this.appService.shortcuts()
        .then(
          async (response) => {
            if (response.ok) {
              const { data } = await response.json();
              this.appList$.next(data);
            }
          },
          (error) => {
            this.appList$.error(error);
            logEvent('ERROR', 'Erro ao listar aplicativos desta Instituição.');
          }
        )
    }
  }

  class InfoPopoverComponent extends AbstractComponent {
    constructor() {
      super();
    }

    connectedCallback() {
      this.innerHTML = `
        <ion-content class="ion-padding">
          <ion-card-subtitle>Payload</ion-card-subtitle>
          <pre>${JSON.stringify(this.data, null, 2)}</pre>
        </ion-content>
      `
    }
  }

  /**
   * Helpers
   */
  async function presentToast(options) {
    const toast = Object.assign(document.createElement('ion-toast'), options || {});
  
    document.body.appendChild(toast);
    return toast.present();
  }

  async function presentLoading(options) {
    const loading = Object.assign(document.createElement('ion-loading'), options || {});
  
    document.body.appendChild(loading);
    await loading.present();

    return loading;
  }

  async function presentPopover(ev, options) {
    const popover = Object.assign(document.createElement('ion-popover'), {
      ...options,
      event: ev,
      translucent: true
    });
    document.body.appendChild(popover);

    await popover.present();

    return popover
  }

  async function presentAlertRadio(header, inputs, buttons) {
    const alert = Object.assign(document.createElement('ion-alert'), {
      header, inputs, buttons
    });
    document.body.appendChild(alert);

    await alert.present();

    return alert
  }

  const toJson = (obj) => JSON.stringify(obj);
  const isEmpty = (str) => String(str).trim().length == 0
  const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

  const stdout = document.querySelector('.stdout');
  const logEvent = (level, message, data) => {
    const now = new Date().toLocaleDateString('pt-br');
    const ionItem = document.createElement('ion-item');

    ionItem.innerHTML = `
      <ion-note slot="start" color="${level == 'ERROR' ? 'danger' : ''}">${level}</ion-note>
      <ion-label class="ion-text-wrap">
        <h6>${now}</h6>
        <p>${message}</p>
      </ion-label>
      ${data ? `<ion-button slot="end" fill="clear">
        <ion-icon slot="icon-only" name="help-circle-outline"></ion-icon>
      </ion-button>` : ''}
    `;

    const button = ionItem.querySelector('ion-button');

    if (button) {
      button.addEventListener('click', (evt) => {
        presentPopover(evt, {
          component: 'app-popover-info',
          componentProps: { data },
          cssClass: 'custom-popover'
        })
      })
    }

    stdout.appendChild(ionItem);
  } 

  /**
   * Main
   */
  customElements.define('app-settings', SettingComponent);
  customElements.define('app-card', AppItemComponent);
  customElements.define('app-list', AppListComponent);
  customElements.define('app-popover-info', InfoPopoverComponent);
});
